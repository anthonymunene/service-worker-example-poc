const express = require("express")
const fs = require('fs')
const https = require('https')
const path = require('path')
const key = fs.readFileSync('./localhost-key.pem');
const cert = fs.readFileSync('./localhost.pem');

const app = express()
const port = 3005

// 
const assets = path.resolve(process.cwd(), 'public');

app.use('/service-worker/service-worker.js', 
express.static(path.join(__dirname, "public/service-worker.js"), {
    maxAge: "0",
    setHeaders: function(res, path, stat){
      res.set('service-worker-allowed', '/')
    }
  }))

app.use('/jl-service-worker-integration/service-worker/service-worker.js', 
express.static(path.join(__dirname, "public/service-worker.js"), {
    maxAge: "0",
    setHeaders: function(res, path, stat){
      res.set('service-worker-allowed', '/')
    }
  }))  

app.use('/service-worker/', express.static(assets))
app.use('/jl-service-worker-integration/service-worker/', express.static(assets)) 
const server = https.createServer({key: key, cert: cert }, app);
app.listen(port, () => {
    console.log(`PWA app listening on port ${port}`)
})
