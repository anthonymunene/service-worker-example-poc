module.exports =  {
    // swSrc: './service-worker.js',
    mode: 'development',
    swDest:'public/service-worker.js',
    globDirectory: 'public/',
    globIgnores: ['**/*.html'],
    runtimeCaching: [
        {
          // MUST be the same as "start_url" in manifest.json
          urlPattern: '/',
          // use NetworkFirst or NetworkOnly if you redirect un-authenticated user to login page
          // use StaleWhileRevalidate if you want to prompt user to reload when new version available
          handler: 'NetworkFirst',
          options: {
            // don't change cache name
            cacheName: 'start-url',
            expiration: {
              maxEntries: 1,
              maxAgeSeconds: 24 * 60 * 60 // 24 hours
            }
          }
        },
        {
          urlPattern: /^https:\/\/www.(?:johnlewis|integration.project4)\.com\/static\/core\/.*\.(?:js)$/i,
          handler: 'StaleWhileRevalidate',
          options: {
            cacheName: 'static-core-js-assets',
            expiration: {
              maxEntries: 32,
              maxAgeSeconds: 24 * 60 * 60 // 24 hours
            }
          }
        },
        {
          urlPattern: /^https:\/\/www.(?:johnlewis|integration.project4)\.com\/static\/core\/.*\.(?:css)$/i,
          handler: 'StaleWhileRevalidate',
          options: {
            cacheName: 'static-core-css-assets',
            expiration: {
              maxEntries: 32,
              maxAgeSeconds: 24 * 60 * 60 // 24 hours
            }
          }
        },   
        {
          urlPattern: /^https:\/\/www.(?:johnlewis|integration.project4)\.com\/browser-performance\/.*\.(?:js)$/i,
          handler: 'StaleWhileRevalidate',
          options: {
            cacheName: 'browser-performance',
            expiration: {
              maxEntries: 32,
              maxAgeSeconds: 24 * 60 * 60 // 24 hours
            }
          }
        },          
        // {
        //   urlPattern: /\/cookies\/.*\.(?:js)$/i,
        //   handler: 'StaleWhileRevalidate',
        //   options: {
        //     cacheName: 'cookie-capture-js-assets',
        //     expiration: {
        //       maxEntries: 32,
        //       maxAgeSeconds: 24 * 60 * 60 // 24 hours
        //     }
        //   }
        // },            
        // {
        //   urlPattern: /^https:\/\/fonts\.(?:googleapis|gstatic)\.com\/.*/i,
        //   handler: 'CacheFirst',
        //   options: {
        //     cacheName: 'google-fonts',
        //     expiration: {
        //       maxEntries: 4,
        //       maxAgeSeconds: 365 * 24 * 60 * 60 // 365 days
        //     }
        //   }
        // },
        // {
        //   urlPattern: /\.(?:eot|otf|ttc|ttf|woff|woff2|font.css)$/i,
        //   handler: 'StaleWhileRevalidate',
        //   options: {
        //     cacheName: 'static-font-assets',
        //     expiration: {
        //       maxEntries: 4,
        //       maxAgeSeconds: 7 * 24 * 60 * 60 // 7 days
        //     }
        //   }
        // },
        {
          urlPattern: /\.(?:jpg|jpeg|gif|png|svg|ico|webp)$/i,
          handler: 'StaleWhileRevalidate',
          options: {
            cacheName: 'static-image-assets',
            expiration: {
              maxEntries: 64,
              maxAgeSeconds: 24 * 60 * 60 // 24 hours
            }
          }
        },
        {
          urlPattern: /\.(?:js)$/i,
          handler: 'StaleWhileRevalidate',
          options: {
            cacheName: 'static-js-assets',
            expiration: {
              maxEntries: 32,
              maxAgeSeconds: 24 * 60 * 60 // 24 hours
            }
          }
        },
        // {
        //   urlPattern: /\.(?:css|less)$/i,
        //   handler: 'StaleWhileRevalidate',
        //   options: {
        //     cacheName: 'static-style-assets',
        //     expiration: {
        //       maxEntries: 32,
        //       maxAgeSeconds: 24 * 60 * 60 // 24 hours
        //     }
        //   }
        // },
        // {
        //   urlPattern: /\.(?:json|xml|csv)$/i,
        //   handler: 'NetworkFirst',
        //   options: {
        //     cacheName: 'static-data-assets',
        //     expiration: {
        //       maxEntries: 32,
        //       maxAgeSeconds: 24 * 60 * 60 // 24 hours
        //     }
        //   }
        // },
        // {
        //   urlPattern: /\/api\/.*$/i,
        //   handler: 'NetworkFirst',
        //   method: 'GET',
        //   options: {
        //     cacheName: 'apis',
        //     expiration: {
        //       maxEntries: 16,
        //       maxAgeSeconds: 24 * 60 * 60 // 24 hours
        //     },
        //     networkTimeoutSeconds: 10 // fall back to cache if api does not response within 10 seconds
        //   }
        // },
        // {
        //   urlPattern: /.*/i,
        //   handler: 'NetworkFirst',
        //   options: {
        //     cacheName: 'others',
        //     expiration: {
        //       maxEntries: 32,
        //       maxAgeSeconds: 24 * 60 * 60 // 24 hours
        //     },
        //     networkTimeoutSeconds: 10
        //   }
        // }
      ]
}
