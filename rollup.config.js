const { generateSW } = require('rollup-plugin-workbox');
const workBoxConfig = require('./src/workbox-config')
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import babel from '@rollup/plugin-babel'
const config = {
    input: './src/service-worker.js',
    output: [
        {
            format: 'iife',
            file: 'public/service-worker.js',
            name: 'serviceWorker'
        }
    ],
    plugins: [
        generateSW(
            workBoxConfig
        ),
        babel({
            // exclude: "node_modules/**", 
            babelHelpers: 'bundled',
            extensions: ['.js', '.jsx', '.es6', '.es', '.mjs', '.vue'],
          }), 
          commonjs(),
          resolve({
            browser: true,
          }),    

    ]

}

export default config
